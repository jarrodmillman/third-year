all:
	latexmk -pdflua request

clean:
	rm request.{aux,dvi,fdb_latexmk,fls,log,out}
